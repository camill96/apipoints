from pymongo import MongoClient
from bottle import post, run
from bson import json_util, objectid
import json
import datetime

points = "uis"
meteor = "meteor"

"""
API Function for new request.
__requestPoints__ get three inputs: <i>jsonFrom</i>, which contain information about request,
<i>userId</i>, which contains Telegram Id of user, who send this request and
<i>token</i> of session.

If <i>token</i> returns id, which equals to input <i>tgId</i> then request will be inserting
to database collection "innopointsRequest" and function return <i>_id</i> of request. Else function return
string message "Error".

<b>Main rule:</b> cannot create request without record of tgId and token session!
"""

@post('/api/requestPoints')
def __requestPoints__(jsonForm, tgId, token):
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionInnopointsRequest = database_points['innopointsRequests']
    collectionSessions = database_meteor['sessions']
    if collectionSessions.find({ "$and" : [{"status": "App", "token": token}]}).count() == 1:
        if collectionUsers.find({ "$and" : [{"profile.tgId": tgId, "services.hashedToken": token}]}).count() == 1:
            request = createRequestDocument(jsonForm, tgId)
            collectionInnopointsRequest.insert_one(request)
            return request.get("_id")
        else:
            json_string =  {
                    u"status": "Error",
                    u"comment": "Token",
                }
            parsed_string = json.dumps(json_string)
            return parsed_string
    else:
        json_string = {
                    u"status": "Error",
                    u"comment": "NotApp",
                }
        parsed_string = json.dumps(json_string)
        return parsed_string



def createRequestDocument(jsonForm, tgId):
    form = json.loads(jsonForm)
    request = {
        "from_user": form['fromUser'],
	    "to_user": form['toUsers'],
	    "innopoint": form['innopoint'],
	    "attachmentsFiles": form['attachmentFiles'],
	    "status": form['status'],
	    "modifiedBy": tgId,
	    "date": datetime.datetime.now()
    }
    return request

run(host='127.0.0.1', port=8080, debug=True)
