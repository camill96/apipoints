from pymongo import MongoClient
from bson import objectid
from bottle import post, run
from json import loads, dumps

"""
API Function to change status of request to 'Complete'
__statusComplete__ get three inputs: <i>tgId</i>, which contains Telegram ID of user,
<i>token</i> of session, which helps to check session and right user and
<i>requestId</i> of variable request.
If <i>tgId</i> returns role != "Admin" or "Moderator" then function return string 'User'.
If <i>token</i> returns id, which doesn't equal to input <i>tgId</i> then function return string 'Error'.
If <i>requestId</i> exists in 'innopolisRequest' collection, then will be changing status to 'Complete' and write that
status was modifying by <i>tgId</i> and function returns string 'ChangedToComplete'. Else returns string 'RequestNotExists'.

<b>Main rule:</b> cannot change status without record of tgId and token session!
"""

points = "uis"
meteor = "meteor"


@post('/api/changeToComplete')
def __statusComplete__(tgId, token, requestId):
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionSessions  = database_meteor['sessions']
    collectionRequest = database_points['innopointsRequests']
    request = objectid.ObjectId(requestId)
    if collectionSessions.find({ "$and" : [{"token": token, "tgId": tgId}]}).count() == 1:
        if collectionSessions.find({ "$and" : [{"status": "App", "token": token}]}).count() == 1:
            if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Admin"}]}).count() == 1 or collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Moderator"}]}).count() == 1:
                if collectionRequest.find({"_id": request}).count() == 1:
                    collectionRequest.update_one({"_id": request},
                                             {"$set":{
                                                    "status": "Complete",
                                                    "modifiedBy": tgId}},
                                             )
                    json_string = {
                        u"status": "Ok",
                        u"comment": "ChangedToComplete",
                    }
                    parsed_string = dumps(json_string)
                    return parsed_string
                else:
                    json_string =  {
                        u"status": "Error",
                        u"comment": "RequestDoesntExist",
                    }
                    parsed_string = dumps(json_string)
                    return parsed_string
            else:
                json_string = {
                    u"status": "Error",
                    u"comment": "User",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string = {
                    u"status": "Error",
                    u"comment": "NotAppSession",
                }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string =  {
                    u"status": "Error",
                    u"comment": "token!=tgId",
                }
        parsed_string = dumps(json_string)
        return parsed_string


run(host='127.0.0.1', port=8080, debug=True)