from bottle import post, run
from pymongo import MongoClient
from bson.json_util import dumps, loads
from bson import objectid

"""
API Function for deleting request from User
"""

points = "uis"
meteor = "meteor"

@post('/api/deleterequest')
def __deleteRequestUser__(tgId, token, requestId):
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionSessions  = database_meteor['sessions']
    collectionRequest = database_points['innopointsRequests']
    request = objectid.ObjectId(requestId)
    if collectionSessions.find({ "$and" : [{"token": token, "tgId": tgId}]}).count() == 1:
        if collectionSessions.find({ "$and" : [{"status": "App", "token": token}]}).count() == 1:
            if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "User"}]}).count() == 1:
                if collectionRequest.find({"_id": request}).count() == 1:
                    collectionRequest.delete_many({"_id": request})
                    json_string = {
                        u"status": "Ok",
                        u"comment": "RequestDeleted",
                    }
                    parsed_string = dumps(json_string)
                    return parsed_string
                else:
                    json_string =  {
                        u"status": "Error",
                        u"comment": "RequestDoesntExist",
                    }
                    parsed_string = dumps(json_string)
                    return parsed_string
            else:
                json_string = {
                    u"status": "Error",
                    u"comment": "NotUser",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string = {
                    u"status": "Error",
                    u"comment": "NotAppSession",
                }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
                    u"status": "Error",
                    u"comment": "token!=tgId",
                }
        parsed_string = dumps(json_string)
        return parsed_string



run(host='127.0.0.1', port=8080, debug=True)