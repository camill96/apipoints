from pymongo import MongoClient
import datetime
import json
import hashlib
from bson.json_util import dumps,loads
from json import load

client = MongoClient('localhost', 27017)
database = client['meteor']
collection_users = database['users']
collectionSessions= database['sessions']
json_string = """ {
  "orderID": 42,
  "customerName": "John Smith",
  "customerPhoneN": "555-1234",
  "orderContents": [
    {
      "productID": 23,
      "productName": "keyboard",
      "quantity": 1
    },
    {
      "productID": 13,
      "productName": "mouse",
      "quantity": 1
    }
  ],
  "orderCompleted": true
} """

parsed_string = json.loads(json_string)

#print parsed_string['customerPhoneN']
#print dumps(l)

json_string = """{
        "_id": 546982015,
        "fromUser": "Andrey Zolin",
        "toUsers": "Andrey Zolin",
        "status": "Process",
        "attachmentFiles": [{
                "file1": "proof1.jpg",
                "file2": "proof2.jpg"
        }],
        "modifiedBy": 546982015,
        "innopoint": [{
            "activityName": "InnoCompetition",
            "activityCategory": "Sport",
            "activitySubcategory": "testSubcategory",
            "amount": 150
        }]
    }"""

#print json.loads(json_string)


points = """ {
        "fromUser": 124732089,
        "toUsers": 124732089,
        "status": "Process",
        "attachmentFiles": [{
            "file1": "proof1.jpg",
            "file2": "proof2.jpg"
        }],
        "modifiedBy": 124732089,
        "innopoint": [{
            "activityName": "Olympiad",
            "activityCategory": "Hackaton",
            "activitySubcategory": "Some_subcategory",
            "amount": 200
        }]
    } """
str = json.loads(points)
collectionSessions.find({ "$and" : [{"status": "App", "token": "7894564676dfgd5468dfg54sdg="}]}).count()
collection_users.find({ "$and": [{"profile.tgId": 515412145, "profile.role": "Admin"}]}).count()

temp = dumps(collectionSessions.find_one({"token": "7894564676dfgd5468dfg54sdg="}))
test = loads(temp)
print test["tgId"]

