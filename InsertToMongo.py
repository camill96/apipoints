database = ""

# Function for creating new document of user collection
def user_document():
    user = {
            "createdAt": datetime.now(),
            "services": [
 				{"when": datetime.now(),
 				 "hashedToken": "12sdfs8251dfgdf68764g="
                }],
            "username": "@alex",
            "profile": {
                      "name": "@zolin",
                      "tgId": 124732089,
 		              "firstName": "Aleksey",
		              "lastName": "Iskhakov",
		              "role": "User",
		              "points": 1000,
		              "studentId": "15B1210",
		              "photoFilename": "IskhakovAleksey.jpg"
 		   }
            }
    return user


def points_document():
    points = {
        "tgId": 124732089,
        "fromUser": 124732089,
        "toUsers": 124732089,
        "status": "Process",
        "attachmentFiles": [{
            "file1": "proof1.jpg",
            "file2": "proof2.jpg"
        }],
        "modifiedBy": 124568974,
        "innopoint": [{
            "activityName": "Sport activities",
            "activityCategory": "Club organization",
            "activitySubcategory": "Average intensity index",
        }],
        "amount": 600
    }
    return points

def logs_document():
    logs = {
               "tgId": 452148632,
               "token": "12sdfs8251dfgdf68764g=",
               "server_code": "somecode",
               "status": "App",
               "dob": datetime.now()
    }
    return logs

from pymongo import MongoClient
from datetime import datetime
client = MongoClient('localhost', 27017)
database = client['meteor']
database_points = client['uis']
collection_points = database_points['innopointsRequests']
collection_user = database['users']
collection_logs = database['sessions']

#Insert user
document_user = user_document()
collection_user.insert_one(document_user)
print document_user.get("_id")

#Insert points
#document_points = points_document()
#collection_points.insert_one(document_points)
#print document_points.get("_id")

#Insert logs
#document_logs = logs_document()
#collection_logs.insert_one(document_logs)











