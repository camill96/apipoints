from bottle import post, run
from pymongo import MongoClient
from bson.json_util import dumps

"""
API Function for getting requests with status "Process"
"""

points = "uis"
meteor = "meteor"

#@post('/api/process')
def __getProcessAdmin__(tgId, token):
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionSessions  = database_meteor['sessions']
    collectionRequest = database_points['innopointsRequests']
    if collectionSessions.find({ "$and" : [{"token": token, "tgId": tgId}]}).count() == 1:
        if collectionSessions.find({ "$and" : [{"status": "App", "token": token}]}).count() == 1:
            if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Admin"}]}).count() == 1 or collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Moderator"}]}).count() == 1:
                r = collectionRequest.find({"status": "Process"})
                l = list(r)
                return dumps(l)
            else:
                json_string = {
                    u"status": "Error",
                    u"comment": "User",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string = {
                    u"status": "Error",
                    u"comment": "NotAppSession",
                }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
                    u"status": "Error",
                    u"comment": "token!=tgId",
                }
        parsed_string = dumps(json_string)
        return parsed_string


run(host='127.0.0.1', port=8080, debug=True)