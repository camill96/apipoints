from pymongo import MongoClient
from bottle import post, run, request, get
from bson import json_util, objectid
import json
import datetime
from json import dumps

points = "uis" # contains 'innopointsRequest' collection
meteor = "meteor" # contains 'users' and 'sessions' collections

def getIdFromToken(token):
    client = MongoClient('localhost', 27017)
    database_meteor = client[meteor]
    collectionSessions = database_meteor['authSessions']
    if collectionSessions.find({ "$and" : [{"status": "App", "token": token}]}).count() == 1:
        return json_util.dumps(collectionSessions.find_one({"token": token}), default=json_util.default)
    else:
        json_string = {
                u"status": "Error",
                u"comment": "NotApp"
        }
        parsed_string = json_util.dumps(json_string, default=json_util.default)
        return parsed_string

"""
API Function for new request.
"""

@post('/api/request/add/<token>')
def __requestPoints__(token):
    jsonForm = request.body.read()
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionInnopointsRequest = database_points['innopointsRequests']
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    if stat == "App":
        tgId = person["tgId"]
        if collectionUsers.find({ "$and" : [{"profile.tgId": tgId, "services.hashedToken": token}]}).count() == 1:
            requests = createRequestDocument(jsonForm, tgId)
            return str(collectionInnopointsRequest.insert_one(requests).inserted_id)
        else:
            json_string =  {
                    u"status": "Error",
                    u"comment": "Token",
                }
            parsed_string = json.dumps(json_string)
            return parsed_string
    else:
        json_string = {
                    u"status": "Error",
                    u"comment": "NotApp",
                }
        parsed_string = json.dumps(json_string)
        return parsed_string



def createRequestDocument(jsonForm, tgId):

    form = json.loads(jsonForm)
    temp = form["innopoint"][0]
    amount = getPoints(temp['activityName'], temp['activityCategory'], temp['activitySubcategory'])
    requests = {
        "from_user": form['fromUser'],
	    "to_user": form['toUsers'],
	    "innopoint": form['innopoint'],
	    "attachmentsFiles": form['attachmentFiles'],
	    "status": form['status'],
	    "modifiedBy": tgId,
	    "date": datetime.datetime.now(),
        "amount": amount
    }
    return requests




"""
API Function for getting requests with status "Process"
"""
@post('/api/status/process/<token>')
def __getProcessAdmin__(token):
    jsonForm = request.body.read()
    limits = json_util.loads(jsonForm)["limit"]
    skips = json_util.loads(jsonForm)["skip"]
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionRequest = database_points['innopointsRequests']
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    if stat == "App":
        tgId = person["tgId"]
        if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Admin"}]}).count() == 1 or collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Moderator"}]}).count() == 1:
             r = collectionRequest.find({"status": "Process"}).limit(limits).skip(skips)
             l = list(r)
             return json_util.dumps(l)
        else:
            json_string = {
                 u"status": "Error",
                 u"comment": "User",
            }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
                u"status": "Error",
                u"comment": "NotApp",
        }
        parsed_string = dumps(json_string)
        return parsed_string


"""
API Function to get person from token
"""
@post('/api/person/<token>')
def __getPerson__(token):
    client = MongoClient('localhost', 27017)
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    if stat == "App":
        tgId = person["tgId"]
        r = collectionUsers.find({"profile.tgId": tgId})
        return json_util.dumps(r, default=json_util.default)
    else:
        json_string ={
                    u"status": "Error",
                    u"comment": "NotApp",
            }
        parsed_string = json_util.dumps(json_string)
        return parsed_string


"""
API Function for getting history of requests of User
"""
@post('/api/history/user/<token>')
def __getHistoryUser__(token):
    client = MongoClient('localhost', 27017)
    jsonForm = request.body.read()
    limits = json_util.loads(jsonForm)["limit"]
    skips = json_util.loads(jsonForm)["skip"]
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionRequest = database_points['innopointsRequests']
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    if stat == "App":
        tgId = person["tgId"]
        if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "User"}]}).count() == 1:
           r = collectionRequest.find({"tgId": tgId}).limit(limits).skip(skips)
           l = list(r)
           return json_util.dumps(l)
        else:
           json_string = {
               u"status": "Error",
               u"comment": "NotUser",
           }
           parsed_string = dumps(json_string)
           return parsed_string
    else:
        json_string = {
                u"status": "Error",
                u"comment": "NotAppSession",
        }
        parsed_string = dumps(json_string)
        return parsed_string



"""
API Function for getting requests with status "Complete" and "Deny"
"""
@post('/api/status/notprocess/<token>')
def __getHistoryAdmin__(token):
    jsonForm = request.body.read()
    client = MongoClient('localhost', 27017)
    limits = json_util.loads(jsonForm)["limit"]
    skips = json_util.loads(jsonForm)["skip"]
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionRequest = database_points['innopointsRequests']
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    if stat == "App":
        tgId = person["tgId"]
        if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Admin"}]}).count() == 1 or collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Moderator"}]}).count() == 1:
            r = collectionRequest.find({"$or": [{"status": "Deny"}, {"status": "Complete"}]}).limit(limits).skip(skips)
            l = list(r)
            return json_util.dumps(l)
        else:
            json_string = {
                u"status": "Error",
                u"comment": "User",
            }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
            u"status": "Error",
            u"comment": "NotApp",
        }
        parsed_string = dumps(json_string)
        return parsed_string

"""
API Function for deleting request from User
"""
@post('/api/request/delete/<token>')
def __deleteRequestUser__(token):
    requestId = request.body.read()
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    collectionRequest = database_points['innopointsRequests']
    requests = objectid.ObjectId(requestId)
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    if stat == "App":
        tgId = person["tgId"]
        if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "User"}]}).count() == 1:
            if collectionRequest.find({"_id": requests}).count() == 1:
                collectionRequest.delete_many({"_id": requests})
                json_string = {
                    u"status": "Ok",
                    u"comment": "RequestDeleted",
                }
                parsed_string = dumps(json_string)
                return parsed_string
            else:
                json_string =  {
                    u"status": "Error",
                    u"comment": "RequestDoesntExist",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string = {
                u"status": "Error",
                u"comment": "NotUser",
            }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
               u"status": "Error",
               u"comment": "NotApp",
        }
        parsed_string = dumps(json_string)
        return parsed_string

"""
API Function to change status of request to 'Complete' with getting Modifications from Admin
"""
@post('/api/request/update/<token>')
def __statusCompleteMod__(token):
    jsonFormMod = request.body.read()
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionRequest = database_points['innopointsRequests']
    collectionUsers = database_meteor['users']
    requestId = json_util.loads(jsonFormMod)["requestId"]
    requests = objectid.ObjectId(requestId)
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    tgTo = getIdfromRequest(requests)
    if stat == "App":
        tgId = person["tgId"]
        if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Admin"}]}).count() == 1 or collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Moderator"}]}).count() == 1:
            if collectionRequest.find({"_id": requests}).count() == 1:
                form = json.loads(jsonFormMod)
                collectionRequest.update_one({"_id": requests},
                                        {"$set":{
                                                "from_user": form['fromUser'],
                                                "to_user": form['toUsers'],
                                                "innopoint": form['innopoint'],
                                                "attachmentsNames": form['attachmentFiles'],
                                                "status": "Complete",
                                                "modifiedBy": tgId}},
                                        )
                json_string = {
                     u"status": "Ok",
                     u"comment": "ChangedToCompleteWithMods",
                }
                parsed_string = dumps(json_string)
                return parsed_string
            else:
                json_string = {
                    u"status": "Error",
                    u"comment": "RequestDoesntExist",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string = {
                u"status": "Error",
                u"comment": "User",
            }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
            u"status": "Error",
            u"comment": "NotApp",
        }
        parsed_string = dumps(json_string)
        return parsed_string

"""
API Function for deny request
"""
@post('/api/request/update/deny/<token>')
def __statusDeny__(token):
    jsonFormMod = request.body.read()
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionRequest = database_points['innopointsRequests']
    collectionUsers = database_meteor['users']
    requestId = json_util.loads(jsonFormMod)["requestId"]
    comment = json_util.loads(jsonFormMod)["comment"]
    requests = objectid.ObjectId(requestId)
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    tgTo = getIdfromRequest(requests)
    if stat == "App":
        tgId = int(person["tgId"])
        fileQuery = json_util.dumps(collectionUsers.find({"profile.tgId": tgTo}))
        temp = json_util.loads(fileQuery)[0]
        am = temp["profile"]
        amount = int(am["points"])
        if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Admin"}]}).count() == 1 or collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Moderator"}]}).count() == 1:
            if collectionRequest.find({"_id": requests}).count() == 1:
                fileTemp = json_util.dumps(collectionRequest.find({"_id": requests}))
                t = json_util.loads(fileTemp)[0]
                oldpoints = int(t["amount"])
                newPoints = amount + oldpoints
                collectionRequest.update_one({"_id": requests},
                                          {"$set":{
                                                 "status": "Deny",
                                                 "modifiedBy": tgId,
                                                 "comment": comment}},
                                          )
                collectionUsers.update({"profile.tgId": tgTo},
                                           {"$set": {
                                               "profile.points": newPoints
                                           }}
                                           )
                json_string = {
                     u"status": "Ok",
                     u"comment": "ChangedToDeny",
                }
                parsed_string = dumps(json_string)
                return parsed_string
            else:
                json_string = {
                    u"status": "Error",
                    u"comment": "RequestDoesntExist",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string = {
                u"status": "Error",
                u"comment": "User",
            }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
               u"status": "Error",
               u"comment": "NotApp",
        }
        parsed_string = dumps(json_string)
        return parsed_string


"""
API Function to change status of request to 'Complete'
"""

@post('/api/request/update/complete/<token>')
def __statusComplete__(token):
    jsonFormMod = request.body.read()
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionRequest = database_points['innopointsRequests']
    collectionUsers = database_meteor['users']
    requestId = json_util.loads(jsonFormMod)["requestId"]
    requests = objectid.ObjectId(requestId)
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    tgTo = getIdfromRequest(requests)
    if stat == "App":
        tgId = int(person["tgId"])
        fileQuery = json_util.dumps(collectionUsers.find({"profile.tgId": tgTo}))
        temp = json_util.loads(fileQuery)[0]
        am = temp["profile"]
        amount = int(am["points"])
        if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Admin"}]}).count() == 1 or collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Moderator"}]}).count() == 1:
            if collectionRequest.find({"_id": requests}).count() == 1:
                fileTemp = json_util.dumps(collectionRequest.find({"_id": requests}))
                t = json_util.loads(fileTemp)[0]
                oldpoints = int(t["amount"])
                newPoints = amount + oldpoints
                collectionRequest.update_one({"_id": requests},
                                          {"$set":{
                                              "status": "Complete",
                                              "modifiedBy": tgId}},
                                          )
                collectionUsers.update({"profile.tgId": tgTo},
                                           {"$set": {
                                               "profile.points": newPoints
                                           }}
                                           )
                json_string = {
                    u"status": "Ok",
                    u"comment": "ChangedToComplete",
                }
                parsed_string = dumps(json_string)
                return parsed_string
            else:
                json_string =  {
                    u"status": "Error",
                    u"comment": "RequestDoesntExist",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string = {
                u"status": "Error",
                u"comment": "User",
            }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
            u"status": "Error",
            u"comment": "NotApp",
            }
        parsed_string = dumps(json_string)
        return parsed_string



"""
API Function to change request for User
"""
@post('/api/request/update/user/<token>')
def __changeRequestUser__(token):
    jsonFormMod = request.body.read()
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionRequest = database_points['innopointsRequests']
    collectionUsers = database_meteor['users']
    requestId = json_util.loads(jsonFormMod)["requestId"]
    requests = objectid.ObjectId(requestId)
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    if stat == "App":
        tgId = person["tgId"]
        if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "User"}]}).count() == 1:
            if collectionRequest.find({"_id": requests}).count() == 1:
                form = json_util.loads(jsonFormMod)
                collectionRequest.update_one({"_id": requests},
                                         {"$set":{
                                               "from_user": form['fromUser'],
                                               "to_user": form['toUsers'],
                                               "innopoint": form['innopoint'],
                                               "attachmentsNames": form['attachmentFiles'],
                                               "status": "Complete",
                                               "modifiedBy": tgId}},
                                         )
                json_string = {
                     u"status": "Ok",
                     u"comment": "RequestChanged",
                }
                parsed_string = dumps(json_string)
                return parsed_string
            else:
                json_string = {
                    u"status": "Error",
                    u"comment": "RequestDoesntExist",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string ={
                u"status": "Error",
                u"comment": "NotUser",
            }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
               u"status": "Error",
               u"comment": "NotApp",
        }
        parsed_string = dumps(json_string)
        return parsed_string

def getPoints(activityName, activityCategory, activitySubcategory):
    client = MongoClient('localhost', 27017)
    database_meteor = client[meteor]
    if activityName == "Sport activities":
        activity = "sportActivities"
    elif activityName == "Art and leisure":
        activity = "artAndLeisure"
    elif activityName == "Academic development":
        activity = "academicDevelopment"
    elif activityName == "Volunteerism":
        activity = "volunteerism"
    elif activityName == "Professional direction":
        activity = "professionalDirection"
    elif activityName == "Tutoring":
        activity = "tutoring"
    elif activityName == "Cooperation with the public relations department (PR)":
        activity = "PR"
    elif activityName == "Cooperation with applicants recruitment department":
        activity = "recruitmentDepartment"
    else:
        "Wrong activity"
    collectionActivity = database_meteor[activity]
    if activitySubcategory != "":
        if collectionActivity.find({ "$and": [{"activityCategory": activityCategory, "activitySubcategory": activitySubcategory}]}).count() == 1:
            fileQuery = json_util.dumps(collectionActivity.find({ "$and": [{"activityCategory": activityCategory, "activitySubcategory": activitySubcategory}]}))
            am = json_util.loads(fileQuery)[0]
            return int(am["amount"])
    else:
        if collectionActivity.find({ "$and": [{"activityCategory": activityCategory}]}).count() == 1:
            fileQuery = json_util.dumps(collectionActivity.find({ "$and": [{"activityCategory": activityCategory}]}))
            am = json_util.loads(fileQuery)[0]
            return int(am["amount"])

def getIdfromRequest(requestId):
    client = MongoClient('localhost', 27017)
    database_meteor = client[points]
    collectionRequest = database_meteor["innopointsRequests"]
    fileQuery = json_util.dumps(collectionRequest.find({"_id": requestId}))
    am = json_util.loads(fileQuery)[0]
    return am["toUsers"]

run(host='127.0.0.1', port=8080, debug=True)