from pymongo import MongoClient
from bottle import post, run
from bson import json_util

"""
API Function to get person from token
"""

points = "uis"
meteor = "meteor"


@post('/api/getPerson')
def __getPerson__(token):
    client = MongoClient('localhost', 27017)
    database_meteor = client[meteor]
    collectionUsers = database_meteor['users']
    json_file = getIdFromToken(token)
    person = json_util.loads(json_file)
    stat = person["status"]
    if stat == "App":
        tgId = person["tgId"]
        if tgId != "NotApp":
            r = collectionUsers.find({"profile.tgId": tgId})
            return json_util.dumps(r, default=json_util.default)
        else:
            json_string ={
                    u"status": "Error",
                    u"comment": "NotApp",
            }
            parsed_string = json_util.dumps(json_string)
            return parsed_string
    else:
        json_string ={
                    u"status": "Error",
                    u"comment": "NotApp",
            }
        parsed_string = json_util.dumps(json_string)
        return parsed_string

def getIdFromToken(token):
    client = MongoClient('localhost', 27017)
    database_meteor = client[meteor]
    collectionSessions  = database_meteor['sessions']
    if collectionSessions.find({ "$and" : [{"status": "App", "token": token}]}).count() == 1:
        return json_util.dumps(collectionSessions.find_one({"token": token}), default=json_util.default)
    else:
        json_string = {
                u"status": "Error",
                u"comment": "NotApp"
        }
        parsed_string = json_util.dumps(json_string, default=json_util.default)
        return parsed_string
