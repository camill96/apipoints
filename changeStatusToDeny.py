from pymongo import MongoClient
from bottle import post, run
from json import loads, dumps
from bson import objectid

"""
API Function for deny request
"""

points = "uis" # contains 'innopointsRequest' collection
meteor = "meteor" # contains 'users' and 'sessions' collections

@post('/api/changeToDeny')
def __statusDeny__(tgId, token, requestId, comment):
    client = MongoClient('localhost', 27017)
    database_points = client[points]
    database_meteor = client[meteor]
    collectionRequest = database_points['innopointsRequests']
    collectionUsers = database_meteor['users']
    collectionSessions = database_meteor['sessions']
    request = objectid.ObjectId(requestId)
    if collectionSessions.find({ "$and" : [{"token": token, "tgId": tgId}]}).count() == 1:
        if collectionSessions.find({ "$and" : [{"status": "App", "token": token}]}).count() == 1:
            if collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Admin"}]}).count() == 1 or collectionUsers.find({ "$and": [{"profile.tgId": tgId, "profile.role": "Moderator"}]}).count() == 1:
                if collectionRequest.find({"_id": request}).count() == 1:
                    collectionRequest.update_one({"_id": request},
                                             {"$set":{
                                                    "status": "Deny",
                                                    "modifiedBy": tgId,
                                                    "comment": comment}},
                                             )
                    json_string = {
                        u"status": "Ok",
                        u"comment": "ChangedToDeny",
                    }
                    parsed_string = dumps(json_string)
                    return parsed_string
                else:
                    json_string = {
                        u"status": "Error",
                        u"comment": "RequestDoesntExist",
                    }
                    parsed_string = dumps(json_string)
                    return parsed_string
            else:
                json_string = {
                    u"status": "Error",
                    u"comment": "User",
                }
                parsed_string = dumps(json_string)
                return parsed_string
        else:
            json_string = {
                    u"status": "Error",
                    u"comment": "NotAppSession",
                }
            parsed_string = dumps(json_string)
            return parsed_string
    else:
        json_string = {
                    u"status": "Error",
                    u"comment": "token!=tgId",
                }
        parsed_string = dumps(json_string)
        return parsed_string


run(host='127.0.0.1', port=8080, debug=True)
